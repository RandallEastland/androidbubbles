# AndroidBubbles

This is a prototype app to demonstrate the functionality of the new [Android Bubbles API]. Feel free to clone it and run it or make changes to test your own ideas. (https://developer.android.com/guide/topics/ui/bubbles).

## Device Requirements

* At the time of this writing (March 2020), the only version of Android that will display bubbles is Android 10 (API level 29, originally known as 'Android Q'). 
* The developer preview of Android 11 was expected to include this same functionality, but as of now does not. It is expected that Android 11 will include this before release.

## App Layout

The app includes a single screen that currently has three buttons:

* Game Started Event
	* Displays a bubble that shows the game has started. 
	* Intended to provide an example that can be extended to any one-time event (game delay, scoring play, game ended, etc.)
	
* Simulate Game
	* This runs through a hypothetical game that updates an existing bubble with new information. 
	* In this example, that information is the current score of the game, which will change randomly every 1.5 seconds.
	* Intended to show that existing bubbles can be updated without having to dismiss and recreate them.
	
* Favorite Player Activity
	* Also a one-time bubble. 
	* This one shows an example of when a user's favorite player does something of interest.
	* Although not included in this prototype, when the bubble is clicked, the expanded view could contain a highlight video to show the play. 

