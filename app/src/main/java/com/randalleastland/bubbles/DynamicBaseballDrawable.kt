package com.randalleastland.bubbles

import android.content.Context
import android.graphics.Bitmap
import android.graphics.Canvas
import android.graphics.Color
import android.graphics.Paint
import android.graphics.drawable.AdaptiveIconDrawable
import android.graphics.drawable.Drawable
import android.graphics.drawable.LayerDrawable
import android.text.TextPaint
import android.util.Log
import android.util.TypedValue

class DynamicBaseballDrawable(
    backgroundDrawable: Drawable,
    foreGroundDrawable: Drawable,
    context: Context,
    text: CharSequence,
    private val text2: String
) : AdaptiveIconDrawable(backgroundDrawable, foreGroundDrawable) {

    companion object {
        private const val DEFAULT_COLOR = Color.WHITE
        private const val DEFAULT_TEXT_SIZE = 20
    }

    var text: CharSequence = text
        set(value) {
            field = value
            invalidateSelf()
        }

    private val mPaint: TextPaint = TextPaint(Paint.ANTI_ALIAS_FLAG)

    init {
        mPaint.color = DEFAULT_COLOR
        mPaint.textAlign = Paint.Align.CENTER
        val textSize = TypedValue.applyDimension(
            TypedValue.COMPLEX_UNIT_DIP,
            DEFAULT_TEXT_SIZE.toFloat(),
            context.resources.displayMetrics
        )
        mPaint.textSize = textSize
    }

    override fun draw(canvas: Canvas) {
        super.draw(canvas)
        //        val bounds = bounds
        //  + mPaint.getFontMetricsInt(null))
        val bounds = (foreground as LayerDrawable).getDrawable(2).bounds
        Log.d(
            "ICON",
            "x,y ${bounds.centerX()}, ${bounds.centerY()} -- ${canvas.width},${canvas.height}"
        )
        canvas.drawText(
            text.toString(),
            bounds.centerX().toFloat(),
            (bounds.centerY() + (mPaint.getFontMetrics(null) / 2)).toFloat(),
            mPaint
        ) // this seems very wrong, but seems to work fine
        val boundsHome = (foreground as LayerDrawable).getDrawable(3).bounds
        canvas.drawText(
            text2,
            boundsHome.centerX().toFloat(),
            (boundsHome.centerY() + (mPaint.getFontMetrics(null) / 2)).toFloat(),
            mPaint
        ) // this seems very wrong, but seems to work fine

    }

    fun getBitmap(): Bitmap {
        Log.d("ICON", "Intrinsic w${intrinsicWidth}, h${intrinsicHeight}")
        val bmp = Bitmap.createBitmap(
            intrinsicWidth,
            intrinsicHeight,
            Bitmap.Config.ARGB_8888
        )
        val canvas = Canvas(bmp)
        setBounds(0, 0, canvas.width, canvas.height)
        draw(canvas)
        return bmp
    }

}
