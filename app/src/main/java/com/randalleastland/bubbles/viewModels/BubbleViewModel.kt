package com.randalleastland.bubbles.viewModels

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.viewModelScope
import com.randalleastland.bubbles.notifications.GameEventNotification
import com.randalleastland.bubbles.notifications.NotificationHelper
import com.randalleastland.bubbles.notifications.PlayerEventNotification
import kotlinx.coroutines.*

abstract class BubbleViewModel(application: Application) : AndroidViewModel(application) {

    abstract fun showBubble(message: String)
    abstract fun updateNotification()
    protected lateinit var notification: NotificationHelper
}

class GameEventViewModel(application: Application) : BubbleViewModel(application) {

    init {
        notification = GameEventNotification(application)
        notification.createNotificationChannels()
    }

    override fun showBubble(message: String) {
        viewModelScope.launch {
            withContext(Dispatchers.Main) {
                with(notification) {
                    if (canBubble())
                        showNotification(message)
                }
            }
        }
    }

    override fun updateNotification() {
        viewModelScope.launch {
            while (!gameOver) {
                val message = updateScore()
                notification.updateNotification(awayScore, homeScore, message)
                delay(2000)
            }
            gameOver = false
            awayScore = 0
            homeScore = 0
        }
    }

    private fun updateScore(): String {
        val whoScored = (1..3).shuffled().first().rem(2)
        if (whoScored == HOME_TEAM) homeScore++ else awayScore++
        return when {
            homeScore == winningScore -> {
                gameOver = true
                "Rockies WIN!!!!"
            }
            awayScore == winningScore -> {
                gameOver = true
                "Mets WIN!!!"
            }
            whoScored == HOME_TEAM -> { "Rockies Score!" }
            else -> { "Mets Score!" }
        }
    }

    companion object {
        private var awayScore = 0
        private var homeScore = 0
        private var winningScore = 10
        private var gameOver = false
        private const val HOME_TEAM = 1
    }
}

class PlayerEventViewModel(application: Application) : BubbleViewModel(application) {
    init {
        notification = PlayerEventNotification(application)
        notification.createNotificationChannels()
    }

    override fun showBubble(message: String) {
        viewModelScope.launch {
            withContext(Dispatchers.Main) {
                with(notification) {
                    if (canBubble())
                        showNotification(message)
                }
            }
        }
    }

    override fun updateNotification() {
        // no-op
    }
}
