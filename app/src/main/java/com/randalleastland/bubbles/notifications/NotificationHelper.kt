package com.randalleastland.bubbles.notifications

import android.app.*
import android.content.Context
import android.graphics.drawable.Icon
import android.os.Build
import android.util.Log
import androidx.annotation.RequiresApi
import com.randalleastland.bubbles.R

@RequiresApi(Build.VERSION_CODES.Q)
abstract class NotificationHelper(private val context: Context) {

    protected val notificationManager: NotificationManager? =
        context.getSystemService(NotificationManager::class.java)
    private lateinit var channel: NotificationChannel
    private var channelName: String = context.getString(R.string.channel_name)
    protected var channelId: String = context.getString(R.string.channel_id)

    abstract fun showNotification(message: String)
    abstract fun updateNotification(m1: Int, m2: Int, message: String)

    protected val person = Person.Builder()
        .setIcon(Icon.createWithResource(context, R.drawable.baseball))
        .setName("")
        .setImportant(true)
        .build()

    fun createNotificationChannels() {
        notificationManager?.let {
            if (it.getNotificationChannel(channelId) == null) {

                channel = NotificationChannel(
                    channelId,
                    channelName,
                    NotificationManager.IMPORTANCE_HIGH
                )

                Log.d("bubble", "Can Bubble: $channel.canBubble()")
                it.createNotificationChannel(channel)
            }
        }
    }

    fun canBubble(): Boolean {
        notificationManager?.let {
            val channel = it.getNotificationChannel(channelId)
            return it.areBubblesAllowed() && channel.canBubble()
        }
        return false
    }
}
