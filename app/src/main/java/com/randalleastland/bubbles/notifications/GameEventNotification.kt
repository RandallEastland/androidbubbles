package com.randalleastland.bubbles.notifications

import android.app.*
import android.content.Context
import android.content.Intent
import android.graphics.drawable.Icon
import android.util.Log
import com.randalleastland.bubbles.DynamicBaseballDrawable
import com.randalleastland.bubbles.R
import com.randalleastland.bubbles.activities.GameEventActivity
import java.lang.Exception

class GameEventNotification(private val context: Context) : NotificationHelper(context) {

    override fun showNotification(message: String) {

        val bubbleIntent = PendingIntent.getActivity(
            context,
            0,
            Intent(context, GameEventActivity::class.java).setAction(Intent.ACTION_VIEW),
            PendingIntent.FLAG_UPDATE_CURRENT)

        val list = context.getDrawable(R.drawable.notification) ?: throw Exception("No drawable")
        val background = context.getDrawable(R.color.colorAccent) ?: throw Exception("No drawable")

        val drawable = DynamicBaseballDrawable(
            background,
            list,
            context,
            awayScore.toString(),
            homeScore.toString())
        Log.d("ICON", "Drawing background ${background.intrinsicHeight}, ${list.intrinsicHeight}, ${drawable.intrinsicHeight}")

         val style = Notification.MessagingStyle(person)
            .addMessage(message, System.currentTimeMillis(), person)

        val builder = Notification.Builder(context, channelId)
            .setBubbleMetadata(
                Notification.BubbleMetadata.Builder()
                    .setDesiredHeight(R.dimen.bubble_height)
                    .setIntent(bubbleIntent)
                    .setIcon(Icon.createWithAdaptiveBitmap(drawable.getBitmap()))
                    .setAutoExpandBubble(false)
                    .setSuppressNotification(false)
                    .build()
            )
            .setCategory(Notification.CATEGORY_STATUS)
            .setSmallIcon(R.color.transparent)
            .setShowWhen(true)
            .setAutoCancel(true)
            .setStyle(style)

        notificationManager?.notify(0, builder.build())
    }

    override fun updateNotification(away: Int, home: Int, message: String) {
        awayScore = away
        homeScore = home
        showNotification(message)
    }

    companion object {
        private var awayScore = 0
        private var homeScore = 0
    }
}
