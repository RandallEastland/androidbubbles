package com.randalleastland.bubbles.activities

import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModelProvider
import com.randalleastland.bubbles.R
import com.randalleastland.bubbles.viewModels.*

class MainActivity : AppCompatActivity() {

    private lateinit var gameEventViewModel: GameEventViewModel
    private lateinit var playerEventViewModel: PlayerEventViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        gameEventViewModel = ViewModelProvider(
            this,
            GameEventViewModelFactory(this.application)
        )
            .get(GameEventViewModel::class.java)

        playerEventViewModel = ViewModelProvider(
            this,
            PlayerEventViewModelFactory(
                this.application
            )
        )
            .get(PlayerEventViewModel::class.java)
    }

    fun gameStartingEventBubble(view: View) {
        gameEventViewModel.showBubble(getString(R.string.play_ball))
    }

    fun scoreUpdateBubble(view: View) {
        gameEventViewModel.updateNotification()
    }

    fun playerEventBubble(view: View) {
        playerEventViewModel.showBubble(getString(R.string.player_event))
    }
}
